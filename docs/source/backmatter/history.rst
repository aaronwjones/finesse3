.. include:: /defs.hrst
.. _history:

History
=======




Finesse beginnings (1997 - 2010)
----------------------------------

|Finesse| has been originally developed by Andreas Freise during his PhD at |GEO600|
(`Frequency domain interferometer simulation with higher-order spatial modes
<https://arxiv.org/abs/gr-qc/0309012>`_). 
The idea for |Finesse| was first raised in 1997, when I (Andreas) was visiting the
Max-Planck-Institute for Quantum Optics in Garching, to assist Gerhard Heinzel with his
work on Dual Recycling at the 30 m prototype interferometer :cite:`Freise00`.  We were
using optical simulations which were rather slow and not very flexible. At the same time
Gerhard Heinzel had developed a linear circuit simulation 'LISO' that used a numerical
algorithm to solve the set of linear equations representing an electronic circuit. The
similarities of the two computational tasks and the outstanding performance of LISO lead
to the idea to use the same methods for an optical simulation. Gerhard Heinzel kindly
allowed me to copy the LISO source code which saved me much time and trouble in the
beginning.

In the following years |Finesse| was continually developed at the University in Hannover
within the GEO project :cite:`geo2010,geo_proposal`. |Finesse| has been frequently
utilised during the commissioning of |GEO600| :cite:`amaldi03_lueck,amaldi03_grote,phdFreise`.


Finesse 1 and 2 (2011 - to date)
--------------------------------

At the University of Birmingham Andreas started a new research group with a focus on
optical technology and interferometer design. Over the years many members of the
research group contributed to or used |Finesse| for their research. In particular, 
PhD student Daniel Brown
became lead programmer and improved the code overall while adding several new features.
With his efforts |Finesse| reached version 1.0 and was made available as open source.
Charlotte Bond became a specialist in modelling higher-order optical modes and ensured
mirror surface maps or strange beam shapes implemented correctly in |Finesse|. Keiko
Kokeyama, Paul Fulda,  Ludovico Carbone and Anna Green have helped making |Finesse| a
useful tool for the Advanced LIGO commissioning team. Mengyao Wang and Rebecca Palmer
and Jan Harms helped Daniel with implementing radiation pressure effects and a full
quantum noise treatment in the two-photon formalism.

Pykat (2014 - to date)
----------------------

Modelling the complex gravitational wave detectors often involves an iterative sequence
of many tasks. From the beginning we have used scripting languages to prepare, run and
post-process |Finesse| simulations, for example using Octave and Matlab
(`<http://www.gwoptics.org/simtools/>`_). In 2014 we could see that Python would become
one of the most common and powerful scripting tools in gravitational wave research. We
therefore started to port our existing Matlab tools and scripts related to optical
modelling to Python. In addition, Daniel Brown wrote a new comprehensive Python wrapper
for running |Finesse|. These tools have been merged and then published as the open source 
package Pykat (`<https://www.gwoptics.org/pykat/>`_) with contributions Philip Jones, Samuel
Rowlinson, Sean Leavey, Anna C.Green and Daniel Töyrä.

Acknowledgements
----------------

In the early days of the |Finesse| development, when the software was not much more than
an idea, many people in the gravitational wave community have helped with feedback, bug
reports and encouragement. Some of them are Seiji Kawamura, Guido Müller, Simon
Chelkowski, Keita Kawabe, Osamu Miyakawa, Gabriele Vajente, Maddalena Mantovani,
Alexander Bunkowski, Rainer Künnemeyer, Uta Weiland, Michaela Malec, Oliver Jennrich,
James Mason, Julien Marque, Mirko Prijatelj, Jan Harms, Oliver Bock, Kentaro Somiya,
Antonio Chiummo, Holger Wittel, Hartmut Grote, Bryan Barr, Sabina Huttner, Haixing Miao,
Benjamin Jacobs, Stefan Ballmer, Nicolas Smith-Lefebvre, Daniel Shaddock and probably
many more not mentioned here.

Gerhard Heinzel greatly supported the original
development; he had the idea of using the LISO routines on interferometer problems and
he provided his code for that purpose. Roland Schilling spent hours with Andreas on the
phone discussing C and Fortran, or interferometers and optics. Ken Strain has been a
constant source of help and support during the initial years of development. Jerome
Degallaix has often helped with suggestions, examples and test results based on his code
OSCAR to further develop and test |Finesse|. Paul Cochrane has made a big difference
with his help on transforming the source code from its messy original form into a more
professional package, including a test-suite, an API documentation and above all a
readable source code.

Last but not least we would like to thank the |GEO600| group, especially Karsten
Danzmann and Benno Willke, who allowed Andreas to work on |Finesse| in parallel to his
experimental work on the GEO site. |Finesse| would not exist without their positive and
open attitude towards science.
