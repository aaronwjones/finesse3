.. include:: ../../../defs.hrst
.. _windows-source-installation:

Windows
=======

On Windows we recommend using Conda to install and build |Finesse|. It is possible to
build |Finesse| without using Conda but it is difficult and therefore not recommended.
|Finesse| must be built using GCC or MSVC. The `MSYS2 <https://www.msys2.org/>`__
build tools can instead be used to compile |Finesse| and these are easily installed via
Conda.

You must have Git installed to access the source code repository. You can install Git in
a variety of ways on Windows. The most common way is by `following these instructions
<https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/>`__

From an `Anaconda Prompt` or `Miniconda Prompt` download the git repository into your
desired directory:

.. code-block:: doscon

    C:\> git clone -b master https://gitlab.com/ifosim/finesse/finesse3.git
    C:\> cd finesse3

The above will checkout the `stable` version of |Finesse| in the ``master`` branch. See
below for details on how to change to different versions such as the most current
development release.

If you haven't already got an environment you would like to use |Finesse| in you can
create a new one using the following commands:

.. code-block:: doscon

    C:\> conda env create -n finesse3 -f environment-win.yml
    C:\> conda activate finesse3

The last step to get |Finesse| to work is to build and install |Finesse| into your
local environment's library path:

.. code-block:: doscon

    C:\> make install-conda

Once you've run this command, you can quickly test if it has worked by running:

.. code-block:: doscon

    C:\> kat3 --version

If you see the |Finesse| version printed, everything should be configured correctly.

Rebuilding |Finesse|
--------------------

Once you have installed |Finesse| from source you can easily get updates and rebuild it
by executing the following commands from the |Finesse| repository directory:

.. code-block:: doscon

    C:\> git pull
    C:\> make clean
    C:\> make install-pep517

This should rebuild |Finesse| and can take some time. For small changes to the source
you can omit ``make clean``.

Occasionally changes to the build tooling will require a complete reset of the local
clone of the |Finesse| repository. To do this, the command ``make realclean`` can be
used above instead of ``make clean``.

Changing |Finesse| versions after installation
----------------------------------------------

Switching versions in |Finesse| involves checking out a different git branch. The
process is similar to above when rebuilding, except you use ``git checkout`` beforehand.
It is recommended to use ``make clean`` when switching branches or commits.

.. code-block:: doscon

    C:\> cd finesse3
    C:\> git checkout [ref]
    C:\> git pull
    C:\> make clean
    C:\> make install-pep517

You can replace ``[ref]`` with one of:

``master``
    The latest stable release.

``develop``
    The latest development release (not necesarily stable or runnable).

e.g. ``3.0a1``
    A particular tag.
