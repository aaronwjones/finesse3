.. include:: ../defs.hrst

.. _packaging:

Packaging Finesse
=================

|Finesse| is currently packaged for `pip <https://pypi.org/>`_ and `conda
<https://conda.io/>`_. |Finesse| requires some system dependencies in addition to those
defined in the ``setuptools`` configuration. See :ref:`requirements_guide` for more
information on |Finesse| dependencies.

Conda
-----

.. todo:: document the packaging procedure for Conda

.. _packaging_wheels:

Wheels
------

Platform-specific `wheels <https://pythonwheels.com/>`__ are built for most Linux
platforms (so-called `manylinux` wheels) and macOS. This procedure is automated as part
of the :ref:`continuous integration <continuous_integration>` pipeline, but it is quite
simple. The Red Hat based Docker image ``manylinux2014`` provided by PyPA is used. The
only upstream dependency is currently ``suitesparse-devel`` which provides the KLU C
headers used by |Finesse|'s simulation module.

Building for 'manylinux'
~~~~~~~~~~~~~~~~~~~~~~~~

The procedure for building a ``bdist_wheel`` for the ``manylinux`` target is:

.. code-block:: console

    $ yum install -y suitesparse-devel
    $ pip wheel . --no-deps -w wheelhouse
    $ auditwheel repair wheelhouse/*.whl

These commands should be run from the source code root on the Docker image. The
``/path/to/pip`` parts should be replaced by the path to pip corresponding to the Python
version being targeted.

Building for Mac OSX
~~~~~~~~~~~~~~~~~~~~

The procedure to make a wheel for OSX is similar. The wheel must of course be built
using OSX, and SuiteSparse must be installed by some means other than ``yum`` (e.g. with
``brew install suite-sparse``). The tool for delocalising the produced wheel is also not
``auditwheel``, but rather ``delocate-wheel`` provided by `delocate
<pypi.org/project/delocate/>`__:

.. code-block:: console

    $ pip wheel . --no-deps -w wheelhouse
    $ delocate-wheel wheelhouse/*.whl
