.. include:: /defs.hrst
.. _shiftedbeam:

Simulating off-axis beams: calculating the shift
------------------------------------------------

Due to its use of the paraxial approximation, simulating an off-axis beam in
|Finesse| requires the inclusion of higher order modes, see e.g. Section 9.2 in
:cite:`LivingReview`. In order to study such a misaligned beam in |Finesse| we
can use a telescopic setup such as depicted in :numref:`fig_shiftbeam_basic`
with two mirrors (:kat:element:`m`) or beamsplitters (:kat:element:`bs`) and use
the ``xbeta`` parameter to add a small mismatch angle :math:`\beta` to the angle
of incidence of both beamsplitters (or mirrors).

.. _fig_shiftbeam_basic:
.. figure:: images/telescope.*
   :align: center
   :width: 80%

   Basic setup

We will derive here the shift in the position of the beam as a function of
(among others) this angle :math:`\beta`. In addition we will also derive the
effect of this parameter on the phase of the beam, resulting from the changed
path length between the laser and the detector. Both formulae will be verified
using |Finesse| simulations in example :ref:`example10`.

In the next section we will discuss the required number of higher order
modes to be included and look at the effects of changing the beam parameters
such as waist size and waist position.

Introduction
^^^^^^^^^^^^

In :numref:`fig_shiftbeam_detail` we have drawn in detail the two different
situations: with and without the tilt :math:`\beta`.

.. _fig_shiftbeam_detail:
.. figure:: images/telescope_detail.*
   :align: center
   :width: 80%

   Detailed setup

The dark red part is the "untilted beam", which has an incident angle
:math:`\alpha` on both of the untilted (dark blue) beamsplitters and traverses a
distance :math:`s` between the beamsplitters.
After rotating both beamsplitters over the small angle :math:`\beta` (light
blue) the beam follows the light red path with corresponding incident angle
:math:`\alpha + \beta` and traverses now a distance :math:`\tilde{s}` between
the beamsplitters. The tilted beam hits the second beamsplitter at a distance
:math:`d` away from where the untilted beam hits it, which results in the
relative vertical shift :math:`\Delta` of the position of the outgoing beam for
which we derive Eq. :eq:`eq_shiftbeam_shift` below

.. math::
   \Delta = s \cdot \sin 2\beta

The total difference in length between the light and dark red paths translates
into a relative phaseshift :math:`\delta\varphi` when detecting the beam at the
right of the telescope, which satisfies Eq. :eq:`eq_shiftbeam_dphi` below

.. math::
   \delta \varphi = \frac{2 s \cdot \sin^2 \beta}{\lambda} \cdot 360^\circ

Calculation of shift in position :math:`\Delta`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Using the sine rule for the triangle enclosed by :math:`d`, :math:`s` and
:math:`\tilde{s}`

.. math::
   \frac{d}{\sin 2\beta} = \frac{s}{\sin \gamma} = \frac{\tilde{s}}{\sin(90^\circ+\alpha-\beta)},
   :label: eq_shiftbeam_sinerule

we can express the length :math:`d` as

.. math::
   d = s \cdot \frac{\sin 2\beta}{\sin \gamma}.
   :label: eq_shiftbeam_length_d

Furthermore, from the total angle of that same triangle we can obtain from

.. math::
   (90^\circ-\alpha-\beta+2\alpha) + 2\beta + \gamma = 180^\circ,

the angle :math:`\gamma`

.. math::
   \gamma = 90^\circ - \alpha - \beta.
   :label: eq_shiftbeam_gamma

We need to calculate :math:`\Delta`:

.. math::
   \Delta = d \cdot \sin (90^\circ-\alpha-\beta) = d \cdot \sin \gamma,
   :label: eq_shiftbeam_Delta

where we used eq. :eq:`eq_shiftbeam_gamma`.
Inserting Eq. :eq:`eq_shiftbeam_length_d` we then obtain the shift

.. math::
   \Delta = s \cdot \sin 2\beta
   :label: eq_shiftbeam_shift

We see that all :math:`\alpha` dependence cancels out. Furthermore, we see that
the shift increases linearly with the distance between the two beamsplitters.
Since for larger values of :math:`\beta` the paraxial approximation requires
more and more higher modes to be included in a simulation, increasing :math:`s`
instead of :math:`\beta` could help simulating the same shift :math:`\Delta`.
This will be discussed further below.

Calculation of difference in path length and :math:`\delta\varphi`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note that there are two parts contributing to the difference in path length:

1. :math:`p_1`: the difference between :math:`\tilde{s}` and :math:`s`
2. :math:`p_2`: a horizontal part since we hit the beamsplitter further to
   the right

:math:`p_1` will have a positive contribution, while :math:`p_2` will have a
negative contribution to the change in path length.

calculation p\ :sub:`1`
"""""""""""""""""""""""

Noting that

.. math::
   \sin \gamma = \sin(90^\circ - \alpha - \beta) = \cos(\alpha + \beta)
   :label: eq_shiftbeam_singamma

we can obtain :math:`\tilde{s}` from the sine rule Eq.
:eq:`eq_shiftbeam_sinerule` to get

.. math::
   \tilde{s} = s \cdot \frac{\sin(90^\circ + \alpha - \beta)}{\sin \gamma}
   = s \cdot \frac{\cos(\alpha - \beta)}{\cos(\alpha + \beta)}
   :label: eq_shiftbeam_stilde

so

.. math::
   p_1 = \tilde{s} - s
   = s \cdot \frac{\cos(\alpha - \beta) - \cos(\alpha + \beta)}{\cos(\alpha + \beta)}
   :label: eq_shiftbeam_p_one

calculation p\ :sub:`2`
"""""""""""""""""""""""

Since part :math:`p_2` shortens the total path, we add an overall minus sign:

.. math::
   p_2 = - d \cdot \cos(90^\circ-\alpha-\beta) = -d \cdot \sin(\alpha + \beta)
   :label: eq_shiftbeam_p_two_d

Using eqs. :eq:`eq_shiftbeam_length_d` and :eq:`eq_shiftbeam_singamma` this becomes

.. math::
   p_2 = - s \cdot \frac{\sin (\alpha + \beta) \sin 2\beta}{\cos(\alpha + \beta)}
   :label: eq_shiftbeam_p_two

combining the two
"""""""""""""""""

Combining eqs. :eq:`eq_shiftbeam_p_one` and :eq:`eq_shiftbeam_p_two` we get

.. math::
   s \cdot  \frac{\cos (\alpha - \beta) - \cos (\alpha + \beta) -
                  \sin (\alpha + \beta) \sin 2\beta}{\cos (\alpha + \beta)}
   :label: eq_shiftbeam_combi_one

We work out the numerator:

.. math::
   \begin{aligned}
   \text{numerator} &= \cos (\alpha - \beta) - \cos (\alpha + \beta) - \sin (\alpha + \beta) \sin 2\beta \\

   &= \cos\alpha \cos\beta + \sin\alpha \sin\beta - \cos\alpha \cos\beta + \sin\alpha \sin\beta - \\
   &  \qquad \qquad (\sin\alpha \cos\beta + \cos\alpha \sin\beta) 2 \sin\beta \cos\beta \\

   &= 2 \sin\alpha \sin\beta -2 (\sin\alpha \sin\beta \cos^2\beta + \cos\alpha \cos\beta \sin^2\beta) \\

   &= 2 \sin\alpha \sin\beta ( 1 - \cos^2\beta) - 2 \cos\alpha \cos\beta \sin^2\beta \\

   &= 2 (\sin\alpha \sin\beta - \cos\alpha \cos\beta) \sin^2\beta \\

   &= -2 \cos(\alpha + \beta) \sin^2 \beta
   \end{aligned}

Putting this into eq. :eq:`eq_shiftbeam_combi_one` we see that the
:math:`\cos(\alpha+\beta)` factor cancels out and we find the total path
difference

.. math::
   p_1 + p_2 = -2 s \cdot \sin^2 \beta
   :label: eq_shiftbeam_pathdiff

Note that again the :math:`\alpha` dependence cancels out.
The relative phase shift can be calculated by dividing the path difference Eq.
:eq:`eq_shiftbeam_pathdiff` by the wavelength :math:`\lambda` and multiplying by
:math:`360^\circ`. We also pick up an extra overall minus sign, cancelling the
one from Eq. :eq:`eq_shiftbeam_pathdiff`, since we define a plane wave
as :math:`\cos(\omega t - k x + \varphi)`, see for example Eq. (1.4) in
:cite:`LivingReview`.
Putting it together we get

.. math::
   \delta \varphi = \frac{2 s \cdot \sin^2 \beta}{\lambda} \cdot 360^\circ
   :label: eq_shiftbeam_dphi

Summary
^^^^^^^

- Both the shift of the beam eq. :eq:`eq_shiftbeam_shift` and the change in
  phase eq. :eq:`eq_shiftbeam_dphi` are independent of the angle :math:`\alpha`
- Given the square in eq. :eq:`eq_shiftbeam_pathdiff` the path length difference is
  typically small but not per se compared to the wavelength :math:`\lambda`,
  leading to a significant phase shift.

Effect on a finite size beam
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The above calculation was done for an infinitesimally narrow beam hitting the
beamsplitter exactly in the midpoint (its rotation point). We show here that the
result is the same for a beam hitting the beamsplitter off-axis.

.. _fig_shiftbeam_finite:
.. figure:: images/telescope_finite.*
   :align: center
   :width: 80%

   Moving the original beamsplitter and original beam left

From :numref:`fig_shiftbeam_finite` we see that – compared to the on-axis case –
the effect of such an off-axis incoming beam can be described by a horizontal
translation of the original beamsplitters (dark blue) together with their beams
(dark red) with respect to the original tilted beamsplitters (light blue) and
their beams (light red). It is clear that

- this does not affect the shift :math:`\Delta` of the beam on the beam detector.
- this does not affect the total path length difference and hence
  :math:`\delta\varphi` since the dark red beam gains a bit on the left but
  loses the same amount on the right.
