.. include:: /defs.hrst

.. _commands:

Commands
========

Commands set some properties of the model or its elements. Some commands may only be
specified once per model, others multiple times.

.. kat:command:: fsig

.. kat:command:: lambda

.. kat:command:: modes

    Note: at least one of ``modes``, ``maxtem``, ``include`` and ``exclude`` must be
    specified.

.. kat:command:: link

    :See Also:

        :kat:element:`space`

.. kat:command:: tem
