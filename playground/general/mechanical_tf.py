from finesse import Model
from finesse.components import Laser, Mirror, SignalGenerator
from finesse.detectors import PowerDetectorDemod1
from finesse.analysis.actions import Xaxis

model = Model()

model.chain(
    Laser("L0", P=1), Mirror("ITM", R=0.99, T=0.01), 1, Mirror("ETM", R=1, T=0, phi=1)
)

model.add(PowerDetectorDemod1("REFL_I", node=model.ITM.p1.o, f=model.fsig.f.ref))
model.add(SignalGenerator("sg", node=model.ETM.mech.z, amplitude=1, phase=0))

model.fsig.f = 1
model.analysis = Xaxis(model.fsig.f, "log", 1, 100e6, 1001)

out = model.run()
out.plot(logx=True, logy=True)
