from finesse import Model
from finesse.components import Beamsplitter, Laser, Mirror

# main interferometer
ifo = Model()
lsr = Laser('lsr')
bsp = Beamsplitter('bsp')
ifo.add([lsr, bsp])
ifo.connect(lsr, 1, bsp, 1, L=16.0)

# XArm of interferometer
xarm = Model()
itmx = Mirror('itmx', Rc=-1934)
etmx = Mirror('etmx', Rc=2245)
xarm.add([itmx, etmx])
xarm.connect(itmx, 2, etmx, 1, L=3994)

# YArm of interferometer
yarm = Model()
itmy = Mirror('itmy', Rc=-1934)
etmy = Mirror('etmy', Rc=2245)
yarm.add([itmy, etmy])
yarm.connect(itmy, 2, etmy, 1, L=3994)

# merge models
ifo.merge(xarm, bsp, 3, itmx, 1, L=5.0)
ifo.merge(yarm, bsp, 2, itmy, 1, L=4.9)

print("Path from lsr->etmx = \n{}".format(ifo.full_path(lsr.nodes.n1o, etmx.nodes.n2o)))
print("Path from lsr->etmy = \n{}".format(ifo.full_path(lsr.nodes.n1o, etmy.nodes.n2o)))

ifo.homs.append('00')
carrier = ifo.build()
