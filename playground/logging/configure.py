from finesse import configure
from finesse.script import parse

configure(log_level="debug")

script = """
laser l1 P=1
space s1 l1.p1 m1.p1
mirror m1 R=0.99 T=0.01
"""

parse(script)
