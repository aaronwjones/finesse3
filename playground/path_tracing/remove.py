import finesse

model = finesse.parse("""
l L0 P=1
s s0 L0.p1 M1.p1
m M1 R=0.9 T=0.1
s s1 M1.p2 M2.p1
m M2 R=0.9 T=0.1
""")
ifo = model.model

ifo.remove(ifo.M1)

print(ifo.path(ifo.L0.p1.o, ifo.M2.p2.o))
print(ifo.M2.p1.i.space)
print(ifo.M2.p1.o.space)