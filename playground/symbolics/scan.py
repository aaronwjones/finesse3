import finesse

finesse.plotting.init()

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

var v 0.99
m ITM R=&v.value T=1-&v.value

xaxis v.value lin 0.01 0.99 1000

pd refl ITM.p1.o
pd trns ITM.p2.o
"""
)

out = model.run()

figures = out.plot()
