# from __future__ import division

import os
import sys

import numpy as np
import tabulate as tb

import finesse
import pykat

from warnings import warning

warning(
    "compare.py is deprecated. See the compare.ipynb for some more recent example code"
)


def load_file(path):
    with open(os.path.abspath(os.path.normpath(path))) as f:
        code = f.read()

    return code


def abs_error(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        func = np.abs
    else:
        func = abs
    return func(x - y)


def relative_error(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        func = np.abs
    else:
        func = abs

    denom = np.maximum(func(x), func(y))
    if denom == 0.0:
        return 0.0
    else:
        return func((x - y) / denom)


def make_finesse2_model(code):
    model = pykat.finesse.kat(kat_code=code)
    model.verbose = False
    return model


def make_finesse3_model(code):
    model = finesse.Model()
    model.parse_legacy(code)
    return model


def get_detectors(model, names=True):
    if isinstance(model, finesse.Model):
        ifo = model
        if names:
            return [det.name for det in ifo.detectors]

        return ifo.detectors

    if isinstance(model, pykat.finesse.kat):
        if names:
            return list(model.detectors.keys())

        return model.detectors


def iter_outputs(
    x, out_old, out_new, dets, table, floatfmt, index=None, any_complex=False
):
    for index, _x in enumerate(x):
        for det in dets:
            row = []
            if _x is not None:
                row.extend([_x, det])
            else:
                row.extend([det])
            old = out_old[det]
            new = out_new[det]
            if _x is not None:
                old = old[index]
                new = new[index]

            if np.result_type(new) == np.complex128:
                old = complex(old)
                new = complex(new)
                r_err_amp = relative_error(abs(new), abs(old))
                abs_err_phase = abs_error(np.angle(new), np.angle(old))
                r_err = None

            else:
                old = old.real

                r_err = relative_error(new, old)
                r_err_amp = None
                abs_err_phase = None

            if any_complex:
                row.extend(
                    [
                        format(old, floatfmt),
                        format(new, floatfmt),
                        r_err,
                        r_err_amp,
                        abs_err_phase,
                    ]
                )
            else:
                row.extend([format(old, floatfmt), format(new, floatfmt), r_err])
            table.append(row)


def compare_0d(out_old, out_new, dets, floatfmt):
    headers = ["Detector", "Finesse 2", "Finesse 3", "relerr"]
    table = []

    any_complex = any(out_new._dtype[d] == np.complex128 for d in dets)
    if any_complex:
        headers.extend(["relerr amp", "abserr phs"])

    iter_outputs(
        [None], out_old, out_new, dets, table, floatfmt, any_complex=any_complex
    )

    return table, headers


def compare_1d(out_old, out_new, dets, floatfmt):
    xparam = out_new.p1
    xp_name = f"{xparam.component.name} {xparam.name}"

    headers = [xp_name, "Detector", "Finesse 2", "Finesse 3", "relerr"]
    table = []

    any_complex = any(out_new._dtype[d] == np.complex128 for d in dets)
    if any_complex:
        headers.extend(["relerr amp", "abserr phs"])

    iter_outputs(
        out_new.x1, out_old, out_new, dets, table, floatfmt, any_complex=any_complex
    )

    return table, headers


def compare(out_old, out_new, dets, floatfmt):
    print(f"axes: {out_new.axes}")
    if not out_new.axes:
        table, headers = compare_0d(out_old, out_new, dets, floatfmt=floatfmt)

    elif out_new.axes == 1:
        table, headers = compare_1d(out_old, out_new, dets, floatfmt=floatfmt)

    else:
        print("Error: multi-dimensional output comparisons not yet implemented.")
        sys.exit(-1)

    return tb.tabulate(
        # table, headers, tablefmt="fancy_grid", missingval="n/a", floatfmt=".4G",
        table,
        headers,
        tablefmt="simple",
        missingval="n/a",
        floatfmt=".4G",
    )


def f2f3_text(code, fmt=" .8g"):
    old = make_finesse2_model(code)
    new = make_finesse3_model(code)

    detectors = get_detectors(new)  # names are same anyway so just use finesse3 model

    out_old = old.run()
    out_new = new.run()

    tb.PRESERVE_WHITESPACE = True

    table = compare(out_old, out_new, detectors, fmt)
    print(table)


def f2f3_plot(code, fig_size=[3, 3]):
    import matplotlib

    # from matplotlib import rc
    import matplotlib.pyplot as plt

    formatter = matplotlib.ticker.EngFormatter(unit="")
    formatter.ENG_PREFIXES[-6] = "u"

    old = make_finesse2_model(code)
    new = make_finesse3_model(code)

    detectors = get_detectors(new)  # names are same anyway so just use finesse3 model

    out_f2 = old.run()
    out_f3 = new.run()

    if out_f3.axes != 1:
        print("Error: comparison plots require a single xaxis")
        sys.exit(-1)

    fig = plt.figure()
    fig.set_size_inches(fig_size)
    from itertools import cycle

    if "axes.color_cycle" in matplotlib.rcParams.keys():
        clist = matplotlib.rcParams["axes.color_cycle"]
        colorcycler = cycle(clist)
    else:
        plist = matplotlib.rcParams["axes.prop_cycle"]
        colorcycler = cycle(plist.by_key()["color"])
    ax1 = fig.add_subplot(111)

    x = out_f2.x
    for det in detectors:
        print(det)
        y1 = out_f2[det]
        y2 = out_f3[det]
        color = next(colorcycler)

        ax1.plot(x, y1, "-", linewidth=2, color=color, label=det)
        ax1.plot(x, y2, "--", linewidth=4, color=color)
    ax1.legend()
    ax1.grid()
    plt.show()


def main():
    path = sys.argv[1]
    try:
        fmt = sys.argv[2]
    except IndexError:
        fmt = " .8g"

    code = load_file(path)

    old = make_finesse2_model(code)
    new = make_finesse3_model(code)

    detectors = get_detectors(new)  # names are same anyway so just use finesse3 model

    out_old = old.run()
    out_new = new.run()

    tb.PRESERVE_WHITESPACE = True

    table = compare(out_old, out_new, detectors, fmt)
    print(table)


if __name__ == "__main__":
    main()
