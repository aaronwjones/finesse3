import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

CODE = """
l L0 1 0 n0

s s0 1 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2

attr ITM Rc -10

gauss gL0 L0 n0 10u 0
maxtem 4

xaxis ITM xbeta lin 0 100u 100

pd R nITM1
"""

model = finesse.Model()
model.parse_legacy(CODE)

# out_3 = model.run()['R']
profile()

import pykat

base = pykat.finesse.kat(kat_code=CODE)
base.verbose = False
# out_2 = base.run()['R']

# import numpy as np

# assert np.allclose(out_2, out_3)

profile("base.run()")
