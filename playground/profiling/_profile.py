import os
import cProfile
import pstats


def profile(statement="model.run()", sort="time", num=25):
    filename = "tmp.prof"
    cProfile.run(statement, filename)
    p = pstats.Stats(filename)
    p.sort_stats(sort)
    p.print_stats(num)

    if os.path.isfile(filename):
        os.remove(filename)
