import finesse

finesse.plotting.init()

model = finesse.parse(
    """
l L0 P=1
s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-2.5
s sCAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=2.5

gauss gL0 L0.p1.o w0=1m z=-0.3

modes maxtem=4

pd trns ETM.p2.o

xaxis gL0.w0 0.001 0.005 100
"""
)

out = model.run()
out.plot()
