# import logging
# import numpy as np
# import re
# import networkx as nx
# import copy
# import warnings
# import contextlib

# from finesse.components.node import NodeType
# from collections import defaultdict, OrderedDict

# # from . import base
# from finesse.simulations.base import BaseSimulation
# from .matrix_digraph import (
#     SRE_copy,
#     SREdense_numeric_inverse,
#     SREDenseInverter,
#     SREPresolvedCSC,
#     DeferredMatrixStore,
#     MatrixStoreTypes,
# )

# LOGGER = logging.getLogger(__name__)


# def abs_sq(arr):
#     return arr.real ** 2 + arr.imag ** 2


# # @canFreeze
# class DigraphSimulationBase(BaseSimulation):
#     """
#     Simulation class for executing configuration models.

#     The `Simulation` class uses the :class:`.Model` to build
#     and fill the sparse matrix and is used as a reference for
#     detectors to compute outputs to return to the user.

#     .. note::
#         Instances of this class **cannot** be copied, they are generated
#         from and operate on a :class:`.Model` object in a particular
#         built state.

#     .. note::
#         The module :mod:`pickle` **cannot** be used on instances of this class.
#     """

#     _submatrix_semantic_map = None

#     def print_matrix(self):
#         raise NotImplementedError()

#     def active(self):
#         return self._submatrix_semantic_map is not None

#     def set_source(self, field_idx, vector):
#         if len(field_idx) == 3:
#             node, freq_idx, subidx = field_idx
#         elif len(field_idx) == 2:
#             node, freq_idx = field_idx
#             subidx = None

#         freq = self.frequencies[freq_idx]
#         Mnode = (node, freq)

#         if subidx is None:
#             assert vector.shape == (self._M_sizes[Mnode],)
#             self._M_seq["rhs"].add(Mnode)
#             self._M_req[Mnode].add("rhs")
#             # put it into matrix form to multiple with the size-1 'rhs' node
#             self._M_edges["rhs", Mnode] = DeferredMatrixStore(
#                 ms_type=MatrixStoreTypes.MATRIX,
#                 matrix=vector.reshape(-1, 1),
#                 needs_build=False,
#             )
#         else:
#             try:
#                 source_vect = self._M_edges["rhs", Mnode]
#             except KeyError:
#                 source_vect = DeferredMatrixStore(
#                     ms_type=MatrixStoreTypes.MATRIX,
#                     matrix=np.zeros((self._M_sizes[Mnode], 1), dtype=complex),
#                     needs_build=False,
#                 )
#                 self._M_seq["rhs"].add(Mnode)
#                 self._M_req[Mnode].add("rhs")
#                 self._M_edges["rhs", Mnode] = source_vect
#             source_vect.matrix[subidx] = vector
#         return

#     @contextlib.contextmanager
#     def component_edge_fill(self, comp, edgestr, f1, f2, conjugate=False):
#         """
#         Returns a dictionary of all the submatrices an element has requested
#         for different connections it needs. The key is::

#             (element, connection_name, ifreq, ofreq)

#         Parameters
#         ----------
#         element
#             The object reference that created the requests.
#         connection_name
#             The string name given to the connection.
#         ifreq
#             Iincoming frequency.
#         ofreq
#             Outgoing frequency.

#         Returns
#         -------
#         dict
#         """
#         key = (comp, edgestr, f1, f2)
#         semantic_dict = self._submatrix_semantic_map[key]
#         Mnode_fr, Mnode_to = semantic_dict["iodx_semantic"]
#         is_diagonal = semantic_dict["is_diagonal"]
#         if Mnode_to in self._M_seq[Mnode_fr]:
#             matrix = self._M_edges[Mnode_fr, Mnode_to]
#         else:
#             self._M_seq[Mnode_fr].add(Mnode_to)
#             self._M_req[Mnode_to].add(Mnode_fr)
#             if not is_diagonal:
#                 shape = (self._M_sizes[Mnode_to], self._M_sizes[Mnode_fr])
#                 matrix = DeferredMatrixStore(
#                     ms_type=MatrixStoreTypes.MATRIX,
#                     matrix=np.empty(shape, dtype=complex),
#                     needs_build=False,
#                 )
#             else:
#                 shape = (self._M_sizes[Mnode_to],)
#                 assert self._M_sizes[Mnode_to] == self._M_sizes[Mnode_fr]
#                 # builds only the diagonal. The matrix simplifier is responsible
#                 # for applying the correct matrix mul rules between
#                 # "diagonal matrices" (which are 1d arrays), scalars, and 2d matrices
#                 matrix = DeferredMatrixStore(
#                     ms_type=MatrixStoreTypes.DIAGONAL,
#                     matrix=np.empty(shape, dtype=complex),
#                     needs_build=False,
#                 )
#             self._M_edges[Mnode_fr, Mnode_to] = matrix
#             # assumes that the getter will be filling this matrix
#         yield matrix
#         # do some transformations or tagging here
#         if conjugate:
#             matrix[:].imag *= -1
#         return

#     def component_edge_get(self, comp, edgestr, f1, f2):
#         """
#         Returns a matrix for the submatrix an element has requested
#         for different connections it needs. The key is::

#             (element, connection_name, ifreq, ofreq)

#         Parameters
#         ----------
#         element : finesse.component.Connector
#             Is the object reference that created the requests
#         connection_name : str
#             String name given to the connection
#         ifreq : finesse.Frequency
#             Incoming frequency
#         ofreq : finesse.Frequency
#             Outgoing frequency

#         this is a context manager, to be used like
#         with sim.component_edge_fill(key) as mat:
#           mat[:] = computations

#         Returns
#         -------
#         matrix
#         """
#         key = (comp, edgestr, f1, f2)
#         semantic_dict = self._submatrix_semantic_map[key]
#         Mnode_fr, Mnode_to = semantic_dict["iodx_semantic"]
#         return self._M_edges[Mnode_fr, Mnode_to]

#     def __enter__(self):
#         """
#         When entered the Simulation object will create the matrix to be used in
#         the simulation.

#         This is where the matrix gets allocated and constructed. It will expect
#         that the model structure does not change after this until it has been
#         exited.
#         """

#         self._M_sizes = dict()
#         self._M_seq = defaultdict(set)
#         self._M_req = defaultdict(set)
#         self._M_edges = dict()
#         self._submatrix_semantic_map = OrderedDict()

#         # add in some edges for the RHS sources
#         self._M_sizes["rhs"] = 1
#         self._M_seq["rhs"].add("rhs")
#         self._M_req["rhs"].add("rhs")
#         self._M_edges["rhs", "rhs"] = DeferredMatrixStore(
#             ms_type=MatrixStoreTypes.SCALAR, matrix=-1, needs_build=False
#         )

#         self._initialise()

#         self.initial_fill()

#         # TODO ddb: this should be chcking audio and that we have quantum calculations going on
#         # TODO ddb: create sparse noise source matrix
#         # TODO ddb: noise calculations probably more generic than just quantum
#         return self

#     def _initialise_submatrices(self):
#         from finesse.components import FrequencyGenerator, Space

#         for n, node_inf in self._node_info.items():
#             # Optical nodes have HOM information, mech/elec don't, all have frequencies though
#             if n.type is NodeType.OPTICAL:
#                 Nsm = node_inf["nfreqs"]
#                 # Neq = node_inf.nhoms
#                 for fidx in range(Nsm):
#                     freq = self.frequencies[fidx]
#                     Mnode_key = (n, freq)
#                     self._M_seq[Mnode_key].add(Mnode_key)
#                     self._M_req[Mnode_key].add(Mnode_key)
#                     self._M_sizes[Mnode_key] = node_inf["nhoms"]
#                     self._M_edges[Mnode_key, Mnode_key] = DeferredMatrixStore(
#                         ms_type=MatrixStoreTypes.SCALAR, matrix=-1, needs_build=False
#                     )

#             elif n.type is NodeType.MECHANICAL or n.type is NodeType.ELECTRICAL:
#                 freq = self.frequencies[0]
#                 Mnode_key = (n, freq)
#                 self._M_seq[Mnode_key].add(Mnode_key)
#                 self._M_req[Mnode_key].add(Mnode_key)
#                 self._M_sizes[Mnode_key] = 1
#                 self._M_edges[Mnode_key, Mnode_key] = DeferredMatrixStore(
#                     ms_type=MatrixStoreTypes.SCALAR, matrix=-1, needs_build=False
#                 )
#             else:
#                 raise Exception("Not handled")

#         # Loop over every edge in the network which represents a bunch of
#         # connections between frequencies and HOMs between two nodes
#         _done = {}
#         for owner in self._edge_owners:
#             if owner in _done:
#                 continue

#             couples_f = isinstance(owner, FrequencyGenerator)

#             # For each connection this element wants...
#             for name in owner._registered_connections:
#                 nio = tuple(
#                     (owner.nodes[_] for _ in owner._registered_connections[name])
#                 )  # convert weak ref (input, output)
#                 # If we are a carrier matrix only compute optics, no AC
#                 if not self.is_audio:
#                     if (
#                         nio[0].type is not NodeType.OPTICAL
#                         or nio[1].type is not NodeType.OPTICAL
#                     ):
#                         continue

#                 # Loop over all the frequencies we can couple between and add
#                 # submatrixes to the overall model
#                 for ifreq in self.frequencies:
#                     for ofreq in self.frequencies:
#                         # for k in range(Nhom):
#                         # For each input and output frequency check if our
#                         # element wants to couple them at this
#                         if couples_f and not owner._couples_frequency(
#                             self, name, ifreq, ofreq
#                         ):
#                             continue
#                         elif not couples_f and ifreq != ofreq:
#                             # If it doesn't couple frequencies and the
#                             # frequencies are different then ignore
#                             continue

#                         iodx_semantic = []  # submatrix indices
#                         iodx = []  # submatrix indices
#                         tags = []  # descriptive naming tags for submatrix key
#                         key_name = re.sub(r"^[^.]*\.", "", name)
#                         key_name = re.sub(r">[^.]*\.", ">", key_name)
#                         key = [owner, key_name]

#                         # Get simulation unique indices for submatrix
#                         # position. How we get these depends on the type of
#                         # the nodes involved
#                         for freq, node in zip((ifreq, ofreq), nio):
#                             if node.type is NodeType.OPTICAL:
#                                 iodx_semantic.append((node, freq))
#                                 iodx.append(self.findex(node, freq.index))
#                                 tags.append(freq.name)
#                                 key.append(freq)
#                             else:
#                                 # Mechanical and electrical don't have multiple
#                                 # freqs, so always use the zeroth frequency index
#                                 iodx_semantic.append((node, self.frequencies[0]))
#                                 iodx.append(self.findex(node, self.frequencies[0]))
#                                 tags.append("audio")
#                                 key.append(None)

#                         assert len(iodx) == 2
#                         assert len(key) == 4

#                         # TODO, there should be a better registry for
#                         # diagonals support
#                         if isinstance(owner, Space):
#                             is_diagonal = True
#                         else:
#                             is_diagonal = False

#                         if tuple(key) not in self._submatrix_semantic_map:
#                             self._submatrix_semantic_map[tuple(key)] = dict(
#                                 iodx_semantic=iodx_semantic, is_diagonal=is_diagonal,
#                             )
#             _done[owner] = True
#         return

#     def __exit__(self, type_, value, traceback):
#         self._unbuild()
#         self._submatrix_semantic_map = None

#     _first_solve = True

#     def solve(self):
#         if not nx.is_frozen(self.model.network):
#             raise Exception("Model has not been built")

#         SRE = SRE_copy((self._M_seq, self._M_req, self._M_edges,))
#         seq, req, edges = SRE

#         # if self._first_solve:
#         #    from .matrix.SRE_graphs import SRE_graph
#         #    SRE_graph('test_network.pdf', SRE)
#         # solve into out_semantic, but also create the usual "out" indexed vector
#         # so that output requests elsewhere in the code can work

#         SREinv = SREdense_numeric_inverse(
#             seq, req, edges, inputs=["rhs"], outputs=seq.keys(),
#         )

#         seq_i, req_i, edges_i = SREinv
#         out_semantic = dict()
#         for Mnode in seq_i["rhs"]:
#             edges_i["rhs", Mnode]
#             out_semantic[Mnode] = edges_i["rhs", Mnode]

#         self._first_solve = False
#         self.out_semantic = out_semantic

#         # TODO, return the correct output type
#         self.out = out_semantic
#         return

#     def _clear_rhs(self):
#         for Mnode_to in self._M_seq["rhs"]:
#             self._M_req[Mnode_to].remove("rhs")
#             del self._M_edges["rhs", Mnode_to]
#         self._M_seq["rhs"].clear()

#         # add the self-edge back in
#         self._M_seq["rhs"].add("rhs")
#         self._M_req["rhs"].add("rhs")
#         self._M_edges["rhs", "rhs"] = DeferredMatrixStore(
#             ms_type=MatrixStoreTypes.SCALAR, matrix=-1, needs_build=False
#         )
#         return

#     def get_DC_out(self, node, freq=0, hom=0):
#         Mnode = (node, self.frequencies[freq])
#         outvec = self.out_semantic[Mnode].matrix
#         return outvec[hom]


# class DigraphSimulation(DigraphSimulationBase):
#     _first_solve = True

#     def solve(self):
#         if not nx.is_frozen(self.model.network):
#             raise Exception("Model has not been built")

#         SRE = SRE_copy((self._M_seq, self._M_req, self._M_edges,))
#         seq, req, edges = SRE

#         SREinv = SREDenseInverter(
#             seq, req, edges, sizes=self._M_sizes, inputs=["rhs"], outputs=seq.keys(),
#         )
#         # if self._first_solve:
#         #    SREinv.plot_graph('test_network_0.pdf')
#         # solve into out_semantic, but also create the usual "out" indexed vector
#         # so that output requests elsewhere in the code can work
#         N = 0
#         while SREinv.simplify_trivial():
#             N += 1
#             # if self._first_solve:
#             #    SREinv.plot_graph('test_network_{}.pdf'.format(N))
#         N += 1
#         # if self._first_solve:
#         #    SREinv.plot_graph('test_network_{}.pdf'.format(N))
#         seq_i, req_i, edges_i = SREinv.numeric_finish()
#         out_semantic = dict()
#         for Mnode in seq_i["rhs"]:
#             edges_i["rhs", Mnode]
#             out_semantic[Mnode] = edges_i["rhs", Mnode]

#         self._first_solve = False
#         self.out_semantic = out_semantic

#         # TODO, return the correct output type
#         self.out = out_semantic


# class DigraphSimulationReducing(DigraphSimulationBase):
#     _first_solve = True
#     _last_edges = None
#     _presolve = None

#     def solve(self):
#         if not nx.is_frozen(self.model.network):
#             raise Exception("Model has not been built")

#         SRE = SRE_copy((self._M_seq, self._M_req, self._M_edges,))
#         seq, req, edges = SRE

#         # fill has just happened, so determine changing edges
#         edges_changed = dict()
#         if self._last_edges is not None:
#             for ekey, edge in edges.items():
#                 edge_last = self._last_edges[ekey]
#                 if np.any(abs_sq(edge - edge_last) > 1e-12):
#                     edges_changed[ekey] = edge

#         # because the fill is in-place, one must copy all of the matrices,
#         # slowslow
#         last_edges = dict()
#         for ekey, Medge in edges.items():
#             last_edges[ekey] = np.copy(Medge)

#         self._last_edges = last_edges

#         if self._presolve is not None:
#             if set(self._presolve.edges_changing.keys()) != set(edges_changed.keys()):
#                 # clear the presolved node
#                 self._presolve = None

#         # print(edges_changed.keys())

#         if self._presolve is None:
#             # if self._first_solve:
#             #    from .matrix.SRE_graphs import SRE_graph
#             #    SRE_graph('test_network.pdf', SRE)
#             # solve into out_semantic, but also create the usual "out" indexed vector
#             # so that output requests elsewhere in the code can work

#             SREinv = SREDenseInverter(
#                 seq,
#                 req,
#                 edges,
#                 sizes=self._M_sizes,
#                 inputs=["rhs"],
#                 outputs=seq.keys(),
#             )
#             SREinv.set_changing_edges(edges_changed)

#             while SREinv.simplify_trivial():
#                 pass

#             self._presolve = SREinv

#         SREinv = copy.copy(self._presolve)
#         SREinv.reinject_changed_edges(edges_changed)
#         while SREinv.simplify_trivial():
#             pass

#         seq_i, req_i, edges_i = SREinv.numeric_finish()
#         out_semantic = dict()
#         for Mnode in seq_i["rhs"]:
#             out_semantic[Mnode] = edges_i["rhs", Mnode]

#         self._first_solve = False
#         self.out_semantic = out_semantic

#         # TODO, return the correct output type
#         self.out = out_semantic
#         return


# class DigraphSimulationReducingCSC(DigraphSimulationBase):
#     _first_solve = True
#     _last_edges = None
#     _presolve = None
#     _use_KLU_offsets = False
#     _check_edge_changes = False

#     def solve(self):
#         if not nx.is_frozen(self.model.network):
#             raise Exception("Model has not been built")

#         SRE = SRE_copy((self._M_seq, self._M_req, self._M_edges,))
#         seq, req, edges = SRE

#         if self._check_edge_changes:
#             # fill has just happened, so determine changing edges
#             edges_changed_set = set()
#             if self._last_edges is not None:
#                 for ekey, edge in edges.items():
#                     edge_last = self._last_edges[ekey]
#                     if np.any(abs_sq(edge - edge_last) > 1e-12):
#                         edges_changed_set.add(ekey)
#         else:
#             edges_changed_set = None

#         if edges_changed_set is not None:
#             edges_changed_big = self.get_changing_edges()
#             if set(edges_changed_big) - set(edges_changed_set):
#                 warnings.warn("Edge changed lists are inefficient")
#             if set(edges_changed_set) - set(edges_changed_big):
#                 warnings.warn("Edges changed lists are inconsistent!")
#         else:
#             edges_changed_set = self.get_changing_edges()

#         edges_changed = {}
#         for ekey in edges_changed_set:
#             edges_changed[ekey] = edges[ekey]

#         # because the fill is in-place, one must copy all of the matrices,
#         # slowslow
#         last_edges = dict()
#         for ekey, Medge in edges.items():
#             last_edges[ekey] = np.copy(Medge)

#         self._last_edges = last_edges

#         if self._presolve is not None:
#             if self._presolve.edges != set(edges_changed.keys()):
#                 # clear the presolved node
#                 # self._presolve = None
#                 pass

#         if edges_changed:
#             # if self._first_solve:
#             #    from .matrix.SRE_graphs import SRE_graph
#             #    SRE_graph('test_network.pdf', SRE)
#             # solve into out_semantic, but also create the usual "out" indexed vector
#             # so that output requests elsewhere in the code can work

#             if self._presolve is None:
#                 SREinv = SREDenseInverter(
#                     seq,
#                     req,
#                     edges,
#                     sizes=self._M_sizes,
#                     inputs=["rhs"],
#                     outputs=set(seq.keys()),
#                 )
#                 SREinv.set_changing_edges(edges_changed)

#                 while SREinv.simplify_trivial():
#                     pass
#                 if self._use_KLU_offsets:
#                     offsets = {}
#                     idx_max = 0
#                     for n, node_inf in self._node_info.items():
#                         Nsm = node_inf["nfreqs"]
#                         Neq = node_inf["nhoms"]
#                         for fidx in range(Nsm):
#                             freq = self.frequencies[fidx]
#                             Mnode = (n, freq)
#                             idx = node_inf["rhs_index"] + fidx * Neq
#                             offsets[Mnode] = idx
#                         idx_max = max(idx, node_inf["rhs_index"] + (fidx + 1) * Neq)
#                     offsets["rhs"] = idx_max
#                 else:
#                     offsets = None

#                 self._presolve = SREPresolvedCSC.from_inverter(
#                     SREinv,
#                     # inputs_offsets = dict(rhs = 0),
#                     outputs_offsets=offsets,
#                     # internal_offsets = dict(offsets),
#                 )

#             # TODO, actually do the reduce here
#             self._presolve.update_solve(edges_changed)
#             self.out_semantic = None
#         else:
#             SREinv = SREDenseInverter(
#                 seq,
#                 req,
#                 edges,
#                 sizes=self._M_sizes,
#                 inputs=["rhs"],
#                 outputs=set(seq.keys()),
#             )
#             # if self._first_solve:
#             #    SREinv.plot_graph('test_network_0.pdf')
#             # solve into out_semantic, but also create the usual "out" indexed vector
#             # so that output requests elsewhere in the code can work
#             N = 0
#             while SREinv.simplify_trivial():
#                 N += 1
#                 # if self._first_solve:
#                 #    SREinv.plot_graph('test_network_{}.pdf'.format(N))
#             N += 1
#             # if self._first_solve:
#             #    SREinv.plot_graph('test_network_{}.pdf'.format(N))
#             seq_i, req_i, edges_i = SREinv.numeric_finish()
#             out_semantic = dict()
#             for Mnode in seq_i["rhs"]:
#                 edges_i["rhs", Mnode]
#                 out_semantic[Mnode] = edges_i["rhs", Mnode]

#             self.out_semantic = out_semantic

#         self._first_solve = False
#         # TODO, return the correct output type
#         self.out = None
#         return

#     def get_DC_out(self, node, freq=0, hom=0):
#         if self._presolve is None:
#             Mnode = (node, self.frequencies[freq])
#             outvec = self.out_semantic[Mnode].matrix
#             return outvec[hom]
#         else:
#             Mnode = (node, self.frequencies[freq])
#             Midx = self._presolve.offsets_out[Mnode]
#             return self._presolve.out[Midx + hom]
