import finesse

model = finesse.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1 L=2.3

m ITM R=0.99 T=0.01 Rc=-2.5
s sCAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=2.4

cav FP ITM.p2.o ITM.p2.i
"""
)
ifo = model.model

trace = ifo.beam_trace(store=False)
q_at_L0 = trace[ifo.L0.p1.o].qx

solution = ifo.utility_trace(q_at_L0, ifo.L0.p1.o, ifo.ETM.p2.o, direction="x")
data = solution._data

for comp, node in zip(data["components"], data["nodes"]):
    if data[comp]["is_space"]:
        L = data[comp]["L"]
    else:
        L = 0

    q = data[node]["q"]
    w = q.beamsize(L + q.z)
    print(f"Beamsize at {node.full_name} = {w} m")
