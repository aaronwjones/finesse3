import numpy as np

from finesse.script import parse
import finesse.tracing.tools as tracetools

model = parse(
    """
l L0 P=3
bs BS R=0.5 T=0.5 L=0 phi=90 alpha=60
bs BSAR1 R=5e-05 T=0.99995 L=0 alpha=-36.6847
bs BSAR2 R=5e-05 T=0.99995 L=0 alpha=36.6847
m ITMAR R=0 T=0.99998 L=2e-05
m ITM R=0.9929625 T=0.007 L=3.75e-05 Rc=[-5580, -5580]
m ETM R=0.9999565 T=6e-06 L=3.75e-05 Rc=[5580, 5580]
m ETMYAR R=0 T=0.9995 L=0.0005
bs ZM1 R=0.9997125 T=0.00025 L=3.75e-05 Rc=[-50, -50]
bs ZM2 R=0.9999625 T=0 L=3.75e-05 Rc=[-82.5, -82.5]
lens ITM_lens f=75
m SRM R=0.8 T=0.2 L=0 phi=17.3 Rc=[580, 580]
m SRMAR R=0 T=0.99999995 L=5e-08
s lpr1 portA=L0.p1 portB=BS.p1 L=10
s BSsub1 portA=BS.p3 portB=BSAR1.p1 L=0.07478 nr=1.44963098985906
s BSsub2 portA=BS.p4 portB=BSAR2.p1 L=0.07478 nr=1.44963098985906
s lBS_ZM1 portA=BS.p2 portB=ZM1.p1 L=70
s lZM1_ZM2 portA=ZM1.p2 portB=ZM2.p1 L=50
s lZM2_ITMlens portA=ZM2.p2 portB=ITM_lens.p1 L=52.5
s lITM_th2 portA=ITM_lens.p2 portB=ITMAR.p1
s ITMsub portA=ITMAR.p2 portB=ITM.p1 L=0.2 nr=3.42009
s LY portA=ITM.p2 portB=ETM.p1 L=10000
s ETMYsub portA=ETM.p2 portB=ETMYAR.p1 L=0.2 nr=3.42009
s lBS_SRM portA=BSAR2.p3 portB=SRM.p1 L=10
s SRMsub portA=SRM.p2 portB=SRMAR.p1 L=0.0749 nr=3.42009
cav cavARM source=ITM.p2.o via=ETM.p1.i
cav cavSRC source=SRM.p1.o via=ITM.p1.i

lambda 1.55e-06
modes none 0
"""
)

ts = tracetools.propagate_beam(model.ITM.p1.o, model.SRM.p1.i, symbolic=True)
ts.animate({model.elements["lZM1_ZM2"].L: np.linspace(20, 70, 100),}, interval=50)
