import finesse

model = finesse.parse(
    """
l L0 P=1

s s0 L0.p1 BS.p1 L=2.3

bs BS R=0.5 T=0.5

s s1 BS.p2 ITMx.p1 L=0.5
s s2 BS.p3 ITMy.p1 L=0.5

m ITMx R=0.99 T=0.01 Rc=-2.5
s sCAVx ITMx.p2 ETMx.p1 L=1
m ETMx R=0.99 T=0.01 Rc=2.4

m ITMy R=0.99 T=0.01 Rc=-2.5
s sCAVy ITMy.p2 ETMy.p1 L=1
m ETMy R=0.99 T=0.01 Rc=2.4

cav CAVx ITMx.p2.o ITMx.p2.i
cav CAVy ITMy.p2.o ITMy.p2.i
"""
)
ifo = model.model

trace = ifo.beam_trace(store=False)
q_at_L0 = trace[ifo.L0.p1.o].qx

xsolution = ifo.utility_trace(q_at_L0, ifo.L0.p1.o, ifo.ETMx.p2.o, direction="x")
xsolution.plot()

ysolution = ifo.utility_trace(q_at_L0, ifo.L0.p1.o, ifo.ETMy.p2.o, direction="x")
ysolution.plot()
