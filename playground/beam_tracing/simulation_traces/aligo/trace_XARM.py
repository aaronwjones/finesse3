import finesse
from finesse.parse import parse

# from pykat import finesse
# from pykat.tools.detecting import all_bp_detectors

finesse.LOGGER.setLevel("INFO")

XARM_MODEL = r"""
l L0 125 0.0 0.0 ni

const nsilica 1.44963098985906
const Mloss 37.5u
const TLX_f 34.5k         # Thermal lens ITMX
const Larm 3994.4692

%%% FTblock Xarm
###########################################################################

# Distance from beam splitter to X arm input mirror
s lx1 4.993 ni nITMX1a

lens ITMX_lens $TLX_f nITMX1a nITMX1b
s sITMX_th2 0 nITMX1b nITMX1

# X arm input mirror
m2 ITMXAR 0 20u 0 nITMX1 nITMXs1
s ITMXsub 0.2 $nsilica nITMXs1 nITMXs2
m1 ITMX 0.014 $Mloss 0.0 nITMXs2 nITMX2
attr ITMX Rc -1934

# X arm length
s LX $Larm nITMX2 nETMX1

# X arm end mirror
m1 ETMX 5u $Mloss 0.0 nETMX1 nETMXs1
s ETMXsub 0.2 $nsilica nETMXs1 nETMXs2
m2 ETMXAR 0 500u 0 nETMXs2 nPTX
attr ETMX Rc 2245
attr ETMX mass 40
attr ITMX mass 40

###########################################################################
%%% FTend Xarm

cav cavXARM ITMX nITMX2 ETMX nETMX1
"""

model = parse(XARM_MODEL, legacy=True)
model.beam_trace()

# base = finesse.kat()
# base.verbose = False
# base.parse(XARM_MODEL)
# base.parse(all_bp_detectors(base))
# base.parse("""
# noxaxis
# yaxis re:im
# """)
# out = base.run()
# print("\nFinesse 2 trace results:")
# print("--------------------------")
# for probe in base.detectors.keys():
#    print(probe[2:] + " qx = {}".format(out[probe]))
