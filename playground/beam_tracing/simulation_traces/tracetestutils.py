import numpy as np
from pykat.tools.detecting import all_bp_detectors

## Work-In-Progress: need to do some more intelligent transforming of Finesse 2 node
##                   names into Finesse 3 nodes - won't work for many node names in
##                   design.kat for example, as they often end in a number/letter combo
def compare_tracing_results(f3_model, f2_model):
    def values_equal(value1, value2, tolerance=1e-14):
        return np.abs(value1 - value2) < tolerance
    def qs_equal(q1, q2):
        # NOTE: ignoring minus sign differences in waist-position for now as it's
        #       not easily translatable between Finesse 2 and 3
        return (values_equal(np.abs(q1.real), np.abs(q2.real)) and
                values_equal(q1.imag, q2.imag))

    # run Finesse 3 tracing
    f3_trace_out = f3_model.beam_trace()
    opt_node_names = [(on.full_name, on) for on in f3_model.optical_nodes]
    # run Finesse 2 tracing
    f2_model.parse(all_bp_detectors(f2_model))
    f2_model.parse("""
    noxaxis
    yaxis re:im
    """)
    f2_out = f2_model.run()
    # retrieve all Finesse 2 bp detector names
    f2_probes = f2_model.detectors.keys()
    transformed_f2_probes = []
    for p in f2_probes:
        if "dump" in p: continue
        if p[-1] != '0' and 'L' not in p:
            transformed_f2_probes.append(
                (p, list(filter(lambda x: f"{p[3:-1]}.n{p[-1]}" in x[0], opt_node_names))[0][1])
            )
        else:
            transformed_f2_probes.append(
                (p, list(filter(lambda x: f"{p[3:]}.n1" in x[0], opt_node_names))[0][1])
            )
    passed = True
    for n in transformed_f2_probes:
        f3_q = n[1].q
        f2_q = f2_out[n[0]]
        if not qs_equal(f2_q, f3_q):
            passed = False
            print(f"\tDifference at {n[0][2:]}/{n[1]}:")
            print(f"\t\tFinesse 2 q = {f2_q}")
            print(f"\t\tFinesse 3 q = {f3_q}")
    return passed
