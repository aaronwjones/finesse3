import finesse
from finesse.detectors import PowerDetector
from finesse.analysis.actions import Xaxis

with open("../../aligo/design.kat", "r") as file:
    katfile = file.read()

model = finesse.Model()
model.parse_legacy(katfile)

ifo = model

ifo.modes(maxtem = 6)
ifo.add(PowerDetector("pd_out", ifo.BS.p4.o))


ifo.run(Xaxis("ITMX.xbeta", 'lin', 0, 100e-6, 100)).plot()
