import finesse

finesse.LOGGER.setLevel("INFO")

model = finesse.parse("""
l laser 1 0 0 nin

s s1 1m nin nEOM1

mod eom1 $fmod 0.01 1 pm nEOM1 nEOM2

s s2 1m nEOM2 nmR1


m mR 0.92167924 0.0783 0 nmR1 nmR2
s s3 12.073 nmR2 n1


m m1AR 0 0.9999991 0 n1 nm11
s sm1 0.1m nm11 nm12 # no substrate
m m1 0.88199075 0.118 0 nm12 n2

s sarm 2999.9 n2 n3

m1 m2 42.9e-6 4.67e-6 $m2phi n3 n4
attr m2 Rc 3464.875


cav cav1 m1 n2 m2 n3

retrace off
maxtem 1
const fmod 6.264264M
const mfmod -6.264264M

const m2phi 0
const mRphi 0
const beta2 0

attr m1 xbeta $beta2

yaxis log abs

ad a0 0 0 0 n1
ad ap 0 0 $fmod n1
ad am 0 0 $mfmod n1

xaxis s3 L lin 10 13 300
""", True)
ifo = model.model

out = model.run()
out.plot()