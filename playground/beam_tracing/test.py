import finesse

ifo = finesse.Model()
ifo.parse(
    """
### L0 -> BS -> YARM -> SRM of ET-LF
### with ITM_lens.f value giving w ~ 10 mm
### at BS.p2 and ZM1, ZM2 flat

# input
l L0 P=1
s l0 L0.p1 BS.p1 L=9

# Main beam splitter at 60 deg input AOI
bs BS T=0.5 L=37.5u alpha=60
s BSsub1 BS.p3 BSAR1.p1 L=0.07478 nr=&nsilica
s BSsub2 BS.p4 BSAR2.p1 L=0.07478 nr=&nsilica
bs BSAR1 R=50u L=0 alpha=-36.6847
bs BSAR2 R=50u L=0 alpha=36.6847

# Y arm telescope
s lBS_ZM2 BS.p2 ZM2.p1 L=24.0
bs ZM2 T=250u L=37.5u
s lZM1_ZM2 ZM2.p2 ZM1.p1 L=13.1
bs ZM1 T=0 L=37.5u
s lZM1_ITMlens ZM1.p2 ITM_lens.p1 L=52.5

lens ITM_lens 98.1
s lITM_th2 ITM_lens.p2 ITMAR.p1 L=0

# Y arm input mirror
m ITMAR R=0 L=20u
s ITMsub ITMAR.p2 ITM.p1 L=0.2 nr=&nsilica
m ITM T=7000u L=37.5u Rc=-5580

# Y arm length
s l_arm ITM.p2 ETM.p1 L=10k

# Y arm end mirror
m ETM T=6u L=37.5u Rc=5580
s ETMsub ETM.p2 ETMAR.p1 L=0.2 nr=&nsilica
m ETMAR R=0 L=500u

# SRC

s lSRC BSAR2.p3 SRM.p1 L=11.585

m SRM T=0.2 L=0
s SRMsub SRM.p2 SRMAR.p1 L=0.0749 nr=&nsilica
m SRMAR R=0 L=50n


# cavities
#cav cavARM ITM.p2.o ITM.p2.i
cav cavSRC SRM.p1.o SRM.p1.i ITM.p1.i

var nsilica 1.44963098985906

lambda 1550n

"""
)

print(ifo.cavSRC.ABCD)
