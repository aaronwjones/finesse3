import finesse

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1
s s0 L0.p1 ITM.p1

m ITM R=0.9 T=0.1 Rc=-5580
s sCAV ITM.p2 ETM.p1 L=10k
m ETM R=0.9 T=0.1 Rc=5580

cav FP ITM.p2.o ITM.p2.i
"""
)

print(ifo.FP.ABCD)
print(ifo.FP.g)
print(ifo.FP.q)
print(ifo.FP.round_trip_gouy)

ifo.ITM.Rc = -5100
ifo.elements["sCAV"].L = 9e3

print(ifo.FP.ABCD)
print(ifo.FP.g)
print(ifo.FP.q)
print(ifo.FP.round_trip_gouy)
