"""Kat file parser tests"""

import unittest

from finesse.parse import KatParser


class KatParserTestCase(unittest.TestCase):
    """Resettable kat parser"""

    def setUp(self):
        self.parser = KatParser()

    def reset(self):
        self.parser = KatParser()


class LaserTestCase(KatParserTestCase):
    def test_laser(self):
        # With phase.
        self.reset()
        self.parser.parse("l l1 1 0 0 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["lasers"][0],
            {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"},
        )
        # Without phase.
        self.reset()
        self.parser.parse("l l1 1 0 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["lasers"][0],
            {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"},
        )


class SpaceTestCase(KatParserTestCase):
    def test_space(self):
        # With n.
        self.reset()
        self.parser.parse("s s1 1 1.45 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["spaces"][0],
            {"name": "s1", "L": 1, "n": 1.45, "node1": "n1", "node2": "n2"},
        )
        # Without n.
        self.reset()
        self.parser.parse("s s1 1 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["spaces"][0],
            {"name": "s1", "L": 1, "n": 1, "node1": "n1", "node2": "n2"},
        )


class MirrorTestCase(KatParserTestCase):
    def test_mirror(self):
        # R / T.
        self.reset()
        self.parser.parse("m m1 0.99 0.01 0 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["mirrors"][0],
            {
                "name": "m1",
                "R": 0.99,
                "T": 0.01,
                "L": None,
                "phi": 0,
                "node1": "n1",
                "node2": "n2",
            },
        )
        # T / Loss.
        self.reset()
        self.parser.parse("m1 m1 0.01 0 0 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["mirrors"][0],
            {
                "name": "m1",
                "R": None,
                "T": 0.01,
                "L": 0,
                "phi": 0,
                "node1": "n1",
                "node2": "n2",
            },
        )
        # R / Loss.
        self.reset()
        self.parser.parse("m2 m1 0.99 0 0 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["mirrors"][0],
            {
                "name": "m1",
                "R": 0.99,
                "T": None,
                "L": 0,
                "phi": 0,
                "node1": "n1",
                "node2": "n2",
            },
        )


class BeamSplitterTestCase(KatParserTestCase):
    def test_beamsplitter(self):
        # R / T.
        self.reset()
        self.parser.parse("bs bs1 0.99 0.01 0 45 n1 n2 n3 n4")
        self.assertDictEqual(
            self.parser.blocks[None]["beamsplitters"][0],
            {
                "name": "bs1",
                "R": 0.99,
                "T": 0.01,
                "L": None,
                "phi": 0,
                "alpha": 45,
                "node1": "n1",
                "node2": "n2",
                "node3": "n3",
                "node4": "n4",
            },
        )
        # T / Loss.
        self.reset()
        self.parser.parse("bs1 bs1 0.01 0 0 45 n1 n2 n3 n4")
        self.assertDictEqual(
            self.parser.blocks[None]["beamsplitters"][0],
            {
                "name": "bs1",
                "R": None,
                "T": 0.01,
                "L": 0,
                "phi": 0,
                "alpha": 45,
                "node1": "n1",
                "node2": "n2",
                "node3": "n3",
                "node4": "n4",
            },
        )
        # R / Loss.
        self.reset()
        self.parser.parse("bs2 bs1 0.99 0 0 45 n1 n2 n3 n4")
        self.assertDictEqual(
            self.parser.blocks[None]["beamsplitters"][0],
            {
                "name": "bs1",
                "R": 0.99,
                "T": None,
                "L": 0,
                "phi": 0,
                "alpha": 45,
                "node1": "n1",
                "node2": "n2",
                "node3": "n3",
                "node4": "n4",
            },
        )


class DirectionalBeamSplitterTestCase(KatParserTestCase):
    def test_directional_beamsplitter(self):
        self.reset()
        self.parser.parse("dbs dbs1 n1 n2 n3 n4")
        self.assertDictEqual(
            self.parser.blocks[None]["directional_beamsplitters"][0],
            {
                "name": "dbs1",
                "node1": "n1",
                "node2": "n2",
                "node3": "n3",
                "node4": "n4",
            },
        )


class ModulatorTestCase(KatParserTestCase):
    def test_modulator(self):
        # No phase
        self.reset()
        self.parser.parse("mod EOM 100 0.1 3 am n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["modulators"][0],
            {
                "name": "EOM",
                "f": 100,
                "midx": 0.1,
                "order": 3,
                "type": "am",
                "phase": 0,
                "node1": "n1",
                "node2": "n2",
            },
        )
        # With phase
        self.reset()
        self.parser.parse("mod EOM 100 0.1 3 am 10 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["modulators"][0],
            {
                "name": "EOM",
                "f": 100,
                "midx": 0.1,
                "order": 3,
                "type": "am",
                "phase": 10,
                "node1": "n1",
                "node2": "n2",
            },
        )


class LensTestCase(KatParserTestCase):
    def test_lens(self):
        self.reset()
        self.parser.parse("lens lens1 1 n1 n2")
        self.assertDictEqual(
            self.parser.blocks[None]["lenses"][0],
            {"name": "lens1", "f": 1, "node1": "n1", "node2": "n2"},
        )


class AmplitudeDetectorTestCase(KatParserTestCase):
    def test_amplitude_detector(self):
        # With mode numbers.
        self.reset()
        self.parser.parse("ad ad1 1 1 0 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["amplitude_detectors"][0],
            {"name": "ad1", "n": 1, "m": 1, "f": 0, "node": "n1"},
        )
        # Without mode numbers.
        self.reset()
        self.parser.parse("ad ad1 0 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["amplitude_detectors"][0],
            {"name": "ad1", "n": None, "m": None, "f": 0, "node": "n1"},
        )


class PhotoDetectorTestCase(KatParserTestCase):
    def test_photo_detector(self):
        # With no demodulation, pd.
        self.reset()
        self.parser.parse("pd pd1 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["photo_detectors"][0],
            {"name": "pd1", "node": "n1"},
        )
        # With no demodulation, pd0.
        self.reset()
        self.parser.parse("pd0 pd1 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["photo_detectors"][0],
            {"name": "pd1", "node": "n1"},
        )
        # With 1 demodulation.
        self.reset()
        self.parser.parse("pd1 pd1 100 0 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["photo_detectors"][0],
            {"name": "pd1", "f1": 100, "phase1": 0, "node": "n1"},
        )
        # With 1 demodulation and no phase.
        self.reset()
        self.parser.parse("pd1 pd1 100 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["photo_detectors"][0],
            {"name": "pd1", "f1": 100, "node": "n1"},
        )
        # With 2 demodulations.
        self.reset()
        self.parser.parse("pd2 pd1 100 0 200 180 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["photo_detectors"][0],
            {
                "name": "pd1",
                "f1": 100,
                "phase1": 0,
                "f2": 200,
                "phase2": 180,
                "node": "n1",
            },
        )
        # With 2 demodulations and no phase.
        self.reset()
        self.parser.parse("pd2 pd1 100 0 200 n1")
        self.assertDictEqual(
            self.parser.blocks[None]["photo_detectors"][0],
            {"name": "pd1", "f1": 100, "phase1": 0, "f2": 200, "node": "n1"},
        )


class CavityTestCase(KatParserTestCase):
    def test_cavity(self):
        self.reset()
        self.parser.parse("cav cav1 m1 n2 m2 n3")
        self.assertDictEqual(
            self.parser.blocks[None]["cavities"][0],
            {
                "name": "cav1",
                "component1": "m1",
                "node1": "n2",
                "component2": "m2",
                "node2": "n3",
            },
        )


class MultilineTestCase(KatParserTestCase):
    def test_multiline(self):
        self.reset()
        self.parser.parse(
            """
            l l1 1 0 0 n1
            s s1 1 n1 n2
            m m1 1 0 0 n2 n3
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["lasers"][0],
            {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"},
        )
        self.assertDictEqual(
            self.parser.blocks[None]["spaces"][0],
            {"name": "s1", "L": 1, "n": 1, "node1": "n1", "node2": "n2"},
        )
        self.assertDictEqual(
            self.parser.blocks[None]["mirrors"][0],
            {
                "name": "m1",
                "R": 1,
                "T": 0,
                "L": None,
                "phi": 0,
                "node1": "n2",
                "node2": "n3",
            },
        )

    def test_multiline_split(self):
        """Test with two calls to parser"""
        self.reset()
        self.parser.parse(
            """
            l l1 1 0 0 n1
            s s1 1 n1 n2
            """
        )
        self.parser.parse("m m1 1 0 0 n2 n3")
        self.assertDictEqual(
            self.parser.blocks[None]["lasers"][0],
            {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"},
        )
        self.assertDictEqual(
            self.parser.blocks[None]["spaces"][0],
            {"name": "s1", "L": 1, "n": 1, "node1": "n1", "node2": "n2"},
        )
        self.assertDictEqual(
            self.parser.blocks[None]["mirrors"][0],
            {
                "name": "m1",
                "R": 1,
                "T": 0,
                "L": None,
                "phi": 0,
                "node1": "n2",
                "node2": "n3",
            },
        )


class ConstantTestCase(KatParserTestCase):
    def test_constant(self):
        self.reset()
        self.parser.parse(
            """
            const power 100
            const name srm
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["constants"], {"$power": 100, "$name": "srm"}
        )


class AttributeTestCase(KatParserTestCase):
    def test_attribute(self):
        self.reset()
        self.parser.parse(
            """
            attr m1 Rcx 10 Rcy 9
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["attributes"], {"m1": [("Rcx", 10), ("Rcy", 9)]}
        )


class VariableTestCase(KatParserTestCase):
    def test_variable(self):
        self.reset()
        self.parser.parse(
            """
            set power l1 P
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["variables"], {"power": ("l1", "P")}
        )


class FunctionTestCase(KatParserTestCase):
    def test_function(self):
        self.reset()
        self.parser.parse(
            """
            func f = ($x - 2) * 4
            func g = ln($x+3) / (8 * $y)
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["functions"],
            {"f": "($x - 2) * 4", "g": "ln($x+3) / (8 * $y)"},
        )


class LockTestCase(KatParserTestCase):
    def test_lock(self):
        self.reset()
        self.parser.parse(
            """
            lock loki $DARM_err 10 1n
            lock* thor $mx1 -3 3u
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["locks"][0],
            {
                "name": "loki",
                "variable": "$DARM_err",
                "gain": 10,
                "accuracy": 1e-9,
                "starred": False,
            },
        )
        self.assertDictEqual(
            self.parser.blocks[None]["locks"][1],
            {
                "name": "thor",
                "variable": "$mx1",
                "gain": -3,
                "accuracy": 3e-6,
                "starred": True,
            },
        )


class PutTestCase(KatParserTestCase):
    def test_put(self):
        self.reset()
        self.parser.parse(
            """
            put l1 P $x1
            put* m1 phi $mx1
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["puts"][0],
            {"component": "l1", "parameter": "P", "variable": "$x1", "starred": False},
        )
        self.assertDictEqual(
            self.parser.blocks[None]["puts"][1],
            {
                "component": "m1",
                "parameter": "phi",
                "variable": "$mx1",
                "starred": True,
            },
        )


class NumberTestCase(KatParserTestCase):
    def test_number(self):
        test_cases = [
            "1",
            "31.425",
            "-1",
            "-64.132",
            "3e5",
            "5.3e6",
            "-5e7",
            "3.1e2",
            "4p",
            "5.3u",
            "6m",
            "+7k",
            "8.5M",
            "4.1G",
            "9.999T",
            "+inf",
        ]
        results = [
            1,
            31.425,
            -1,
            -64.132,
            3e5,
            5.3e6,
            -5e7,
            3.1e2,
            4e-12,
            5.3e-6,
            6e-3,
            7e3,
            8.5e6,
            4.1e9,
            9.999e12,
            float("+inf"),
        ]
        for case, result in zip(test_cases, results):
            self.reset()
            self.parser.parse("lens lens1 {} n1 n2".format(case))
            self.assertDictEqual(
                self.parser.blocks[None]["lenses"][0],
                {"name": "lens1", "f": result, "node1": "n1", "node2": "n2"},
            )


class FTblockTestCase(KatParserTestCase):
    def test_FTblock(self):
        self.reset()
        self.parser.parse(
            r"""
            l l1 1 0 n1
            s s1 1 n1 n2

            %%% FTblock MIRROR
            m m1 1 0 0 n2 n3
            %%% FTend MIRROR
            """
        )
        self.assertDictEqual(
            self.parser.blocks[None]["lasers"][0],
            {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"},
        )
        self.assertDictEqual(
            self.parser.blocks[None]["spaces"][0],
            {"name": "s1", "n": 1, "L": 1, "node1": "n1", "node2": "n2"},
        )
        self.assertDictEqual(
            self.parser.blocks["MIRROR"]["mirrors"][0],
            {
                "name": "m1",
                "R": 1,
                "T": 0,
                "L": None,
                "phi": 0,
                "node1": "n2",
                "node2": "n3",
            },
        )


class ParserReuseTestCase(KatParserTestCase):
    def test_different_models(self):
        ifo1 = """
            l l1 1 0 0 n1
            %%% FTblock mirror
            s s1 1 n1 n2
            m m1 1 0 0 n2 n3
            %%% FTend mirror
            """

        ifo2 = """
            l l1 2 0 0 n1
            s s1 3 n1 n2
            m m1 4 0 0 n2 n3
            """

        # parse first ifo
        self.parser.parse(ifo1)

        # parse second ifo with same parser, but with reset state
        self.parser.reset()
        self.parser.parse(ifo2)
        res2a = self.parser.blocks

        # parse second ifo using a newly instantiated parser
        self.reset()
        self.parser.parse(ifo2)
        res2b = self.parser.blocks

        self.assertDictEqual(res2a, res2b)

    def test_same_model(self):
        ifo1 = """
            l l1 1 0 0 n1
            """

        ifo2 = """
            %%% FTblock mirror
            s s1 1 n1 n2
            m m1 1 0 0 n2 n3
            %%% FTend mirror
            """

        # parse first and second parts together
        self.parser.parse(ifo1 + ifo2)
        res1a = self.parser.blocks

        # parse first and second parts subsequently
        self.reset()
        self.parser.parse(ifo1)
        self.parser.parse(ifo2)
        res1b = self.parser.blocks

        self.assertDictEqual(res1a, res1b)
