"""Modal computation module

This module contains highly optimised code for each component for conducting modal simulations.
Component workspaces should register these fill and calculation methods as required.
"""
