"""
Simple test to see if a variable can be added and then used in another model parameter
"""

import finesse

kat = finesse.Model()
kat.parse_legacy(
    """
l l1 0 0 n0
pd pd1 n0
"""
)


x = finesse.components.Variable("x", 10)
kat.add(x)

kat.l1.P = x.value.ref ** 2
