%%% FTblock laser
#####################################################################
l L0 3 0 nlaser1

#####################################################################
%%% FTend laser

s lpr1 10 nlaser1 nPRBS

%%% FTblock BS
#####################################################################
# BS beamsplitter
##                             ^
##                  to IMY     |
##                             |      ,'-.
##                             |     +    `.
##                        nYBS |   ,'       :'
##      nPR3b                  |  +i1      +
##         ---------------->    ,:._  i2 ,'
##    from the PRC       nPRBS + \  `-. + nXBS
##                           ,' i3\   ,' --------------->
##                          +      \ +     to IMX
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nSRBS
##                             -   |
##                                 |to the SRC
##                                 |
##                                 v

### Main beam splitter at 60 deg input AOI
bs1 BS 0.5 0 90 60 nPRBS nYBS nBSi1 nBSi3
s BSsub1 0.07478 $nsilica nBSi1 nBSi2
s BSsub2 0.07478 $nsilica nBSi3 nBSi4
bs2 BSAR1 50u 0 0 -36.6847 nBSi2 dump14 nXBS nPOX
bs2 BSAR2 50u 0 0 36.6847 nBSi4 dump15 nSRBS dump16

#####################################################################
%%% FTend BS

%%% FTblock Yarm
#####################################################################

# Y arm telescope
s lBS_ZM1 70 nYBS nZM1_i
bs1 ZM1 250u 37.5u 0 0 nZM1_i nZM1_o dump dump
attr ZM1 Rc -50
s lZM1_ZM2 50 nZM1_o nZM2_i
bs1 ZM2 0 37.5u 0 0 nZM2_i nZM2_o dump dump
attr ZM2 Rc -82.5
s lZM2_ITMlens 52.5 nZM2_o nITM1a

lens ITM_lens 75 nITM1a nITM1b
s lITM_th2 0 nITM1b nITM1

# Y arm input mirror
m2 ITMAR 0 20u 0 nITM1 nITMs1
s ITMsub 0.2 $nsilicon nITMs1 nITMs2
m1 ITM $T_ITM $L_HR $phi_ITMY nITMs2 nITM2
attr ITM Rc -5580

# Y arm length
s LY $Larm nITM2 nETM1

# Y arm end mirror
m1 ETM $T_ETM $L_HR $phi_ETMY nETM1 nETMs1
s ETMYsub 0.2 $nsilicon nETMs1 nETMs2
m2 ETMYAR 0 500u 0 nETMs2 nPTY
attr ETM Rc 5580

#####################################################################
%%% FTend Yarm

%%% FTblock SRC
#####################################################################
s lBS_SRM 10 nSRBS nSRM1

m1 SRM $T_SR 0 $phi_SRM nSRM1 nSRMs1
attr SRM Rc 580
s SRMsub 0.0749 $nsilicon nSRMs1 nSRMs2
m2 SRMAR 0 50n 0 nSRMs2 nSRMOut

#####################################################################
%%% FTend SRC

%%% FTblock cavities
###########################################################################
cav cavARM ITM nITM2 ETM nETM1
cav cavSRC SRM nSRM1 ITM nITMs2

###########################################################################
%%% FTend cavities

%%% FTblock constants
#####################################################################
const T_ITM 7000E-6
const T_ETM 6E-6
const T_SR 0.2
const L_HR 37.5E-6
const nsilica 1.44963098985906
const nsilicon 3.42009
const Larm 10k
const SLY_f inf
#####################################################################
%%% FTend constants

%%% FTblock tunings
#####################################################################
const phi_SRM 17.3
const phi_ITMX 0.0
const phi_ETMX 0.0
const phi_ITMY 0.0
const phi_ETMY 0.0
const phi_BS 0
#####################################################################
%%% FTend tunings

lambda 1550n
