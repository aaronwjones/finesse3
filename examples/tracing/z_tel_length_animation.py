import numpy as np

import finesse
import finesse.tracing.tools as tracetools

finesse.configure(plotting=True)

IFO = finesse.Model()
IFO.parse_legacy_file("base_LF.kat")

ts = tracetools.propagate_beam(IFO.ITM.p1.o, IFO.SRM.p1.i, symbolic=True)

ts.animate(
    {IFO.elements["lZM1_ZM2"].L: np.linspace(20, 70, 100),}, interval=50,
)
